{
    "name": "VRACOOP - Customisation Pack",
    "summary": "VRACOOP - Customisation Pack",
    "version": "12.0.1.0.1",
    "development_status": "Beta",
    "author": "Le Filament",
    "maintainers": ["remi-filament"],
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "website", "website_sale", "website_sale_delivery", "product_pack"
    ],
    "data": [
        'security/ir.model.access.csv',
        'report/sale_report.xml'
    ]
}
